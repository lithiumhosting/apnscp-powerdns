<?php

protected function zoneAxfr($domain): ?string
{
    $domain = rtrim($domain, '\.');
    // @todo hold records in cache and synthesize AXFR

    try
    {
        $api = $this->makeApi();
        $records = $api->do('GET', "zones/${domain}");

        if (empty($records['rrsets']))
        {
            // No Records Exist
            return null;
        }
        $soa = array_get($this->get_records_external('', 'soa', $domain, $this->get_hosting_nameservers($domain)), 0, []);

        $ttldef   = (int) array_get(preg_split('/\s+/', $soa['parameter'] ?? ''), 6, static::DNS_TTL);
        $preamble = [];
        if ($soa)
        {
            $preamble = [
                "${domain}.\t${ttldef}\tIN\tSOA\t${soa['parameter']}",
            ];
        }
        foreach ($this->get_hosting_nameservers($domain) as $ns)
        {
            $preamble[] = "${domain}.\t${ttldef}\tIN\tNS\t${ns}.";
        }
    }
    catch (ClientException $e)
    {
        if ($e->getResponse()->getStatusCode() === 422)
        {
            return null; // This really shouldn't happen
        }
        if ($e->getResponse()->getStatusCode() === 404)
        {
            return null; // No zone here!
        }

        error("Failed to transfer DNS records from PowerDNS - try again later. Response code: %d", $e->getResponse()->getStatusCode());

        return null;
    }
    foreach ($records['rrsets'] as $r)
    {
        foreach ($r['records'] as $record)
        {
            switch ($r['type'])
            {
                case 'CAA':
                    // @XXX flags always defaults to "0"
                    $parameter = '0 ' . ' ' . $record['content'];
                    break;
                case 'SRV':
                    $parameter = $record['content'];
                    break;
                case 'MX':
                    $parameter = $record['content'];
                    break;
                default:
                    $parameter = $record['content'];
            }
            $preamble[] = $r['name'] . "\t" . $r['ttl'] . "\tIN\t" . $r['type'] . "\t" . $parameter;
        }
    }
    $axfrrec = implode("\n", $preamble);

    return $axfrrec;
}